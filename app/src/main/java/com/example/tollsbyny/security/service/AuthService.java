package com.example.tollsbyny.security.service;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Bundle;

import com.example.tollsbyny.ToolsByNyApp;
import com.example.tollsbyny.client.AuthClient;
import com.example.tollsbyny.security.account.NjEzPassAccount;

public class AuthService {

    private final static String REFRESH_TOKEN = "refresh_token";
    private final static String END_TIME_TOKEN = "end_time_token";
    private final static float END_TIME_FACTOR = 0.8f;
    private final static AccountManager ACCOUNT_MANAGER = AccountManager.get(ToolsByNyApp.getContext());

    private static String TOKEN;

    public static boolean isAccountExists() {
        return ACCOUNT_MANAGER.getAccountsByType(NjEzPassAccount.TYPE).length == 1;
    }

    public static Account getAccount() {
        return ACCOUNT_MANAGER.getAccountsByType(NjEzPassAccount.TYPE)[0];
    }

    public static void addAccount(NjEzPassAccount account, String password, String token, String refreshToken, Integer expiresIn) {

        long endTimeToken = System.currentTimeMillis() + expiresIn;

        ACCOUNT_MANAGER.addAccountExplicitly(account, password, new Bundle());
        ACCOUNT_MANAGER.setAuthToken(account, account.type, token);
        ACCOUNT_MANAGER.setUserData(account, REFRESH_TOKEN, refreshToken);
        ACCOUNT_MANAGER.setUserData(account, END_TIME_TOKEN, Long.toString(endTimeToken));
    }

    public static void addToken(String token) {
        TOKEN = token;
    }

    public static String getAuthToken() {
        if (isAccountExists()) {

            if (isAccountTokenExpired())
                refreshToken();

            return ACCOUNT_MANAGER.peekAuthToken(getAccount(), NjEzPassAccount.TYPE);
        } else {
            return TOKEN;
        }
    }

    public static void clearAccountManager() {

        for (Account account : ACCOUNT_MANAGER.getAccountsByType(NjEzPassAccount.TYPE)) {
            ACCOUNT_MANAGER.removeAccount(account, null, null, null);
        }
    }

    private static void refreshToken() {
        AuthClient client = new AuthClient();
        Account account = getAccount();

        String token = client.refreshToken(ACCOUNT_MANAGER.getUserData(account, REFRESH_TOKEN)).getAccessToken();

        ACCOUNT_MANAGER.setAuthToken(account, account.type, token);
    }

    private static boolean isAccountTokenExpired() {

        float currentTime = System.currentTimeMillis();
        float endTime = Long.valueOf(ACCOUNT_MANAGER.getUserData(getAccount(), END_TIME_TOKEN));

        return (currentTime >= endTime || ((currentTime/endTime) < END_TIME_FACTOR));
    }
}
