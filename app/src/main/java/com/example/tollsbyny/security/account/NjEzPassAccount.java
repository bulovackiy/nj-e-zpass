package com.example.tollsbyny.security.account;

import android.accounts.Account;
import android.os.Parcel;

public class NjEzPassAccount extends Account {

    public static final String TYPE = "com.example.tollsbyny";

    public static final String TOKEN_FULL_ACCESS = "com.github.tollsbyny.TOKEN_FULL_ACCESS";

    public static final String KEY_PASSWORD = "com.github.tollsbyny.KEY_PASSWORD";

    public NjEzPassAccount(Parcel in) {
        super(in);
    }

    public NjEzPassAccount(String name) {
        super(name, TYPE);
    }
}
