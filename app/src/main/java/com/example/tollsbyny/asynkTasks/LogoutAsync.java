package com.example.tollsbyny.asynkTasks;

import android.content.Intent;
import android.os.AsyncTask;

import com.example.tollsbyny.activity.ApplicationActivity;
import com.example.tollsbyny.activity.LoginActivity;
import com.example.tollsbyny.security.service.AuthService;

import java.lang.ref.WeakReference;

public class LogoutAsync extends AsyncTask<Void, Void, Void> {

    private WeakReference<ApplicationActivity> applicationActivityWeakReference;

    public LogoutAsync(ApplicationActivity activity) {
        this.applicationActivityWeakReference = new WeakReference<>(activity);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
    }

    @Override
    protected Void doInBackground(Void... voids) {
        ApplicationActivity activity = applicationActivityWeakReference.get();

        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivityForResult(intent, 111);

        //AuthService.clearAccountManager();

        return null;
    }
}
