package com.example.tollsbyny.asynkTasks;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.Response;

public class ResponseBodyAsync extends AsyncTask<Response, Void, String> {

    @Override
    protected String doInBackground(Response... responses) {
        try {
            return responses[0].body().string();
        } catch (IOException e) {
            return "";
        }
    }
}
