package com.example.tollsbyny.asynkTasks;

import android.os.AsyncTask;

import com.example.tollsbyny.client.UserClient;
import com.example.tollsbyny.fragment.AddVehicleResult;
import com.example.tollsbyny.fragment.NewAddVehicleFragment;
import com.example.tollsbyny.model.lookup.VehicleMake;

import java.util.List;

public class InitVehicleMakesAsync extends AsyncTask<String, Void, List<VehicleMake>> {

    private NewAddVehicleFragment fragment;

    public InitVehicleMakesAsync(NewAddVehicleFragment fragment) {
        this.fragment = fragment;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        fragment.showVehicleMakeProgressBar();
    }

    @Override
    protected void onPostExecute(List<VehicleMake> vehicleMakes) {
        super.onPostExecute(vehicleMakes);
        if (fragment.isVisible()) {
            fragment.initMakeSpinner(vehicleMakes);
            fragment.dissmissVehicleMakeProgressBar();
        }
    }

    @Override
    protected List<VehicleMake> doInBackground(String... strings) {

        UserClient client = new UserClient();

        return client.getVehicleMakes(strings[0]);
    }
}
