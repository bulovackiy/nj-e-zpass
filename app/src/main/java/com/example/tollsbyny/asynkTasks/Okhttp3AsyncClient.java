package com.example.tollsbyny.asynkTasks;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.*;

public class Okhttp3AsyncClient extends AsyncTask<Request, Void, Response> {

    @Override
    protected Response doInBackground(Request... requests) {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(180, TimeUnit.SECONDS)
                .connectionPool(new ConnectionPool(0, 1, TimeUnit.SECONDS))
                .readTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS)
                .build();
        try {
            return client.newCall(requests[0]).execute();
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);
    }
}
