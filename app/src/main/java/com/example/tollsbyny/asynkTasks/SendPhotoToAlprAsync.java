package com.example.tollsbyny.asynkTasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.tollsbyny.activity.ApplicationActivity;
import com.example.tollsbyny.client.AlprClient;
import com.example.tollsbyny.fragment.AddVehicleResult;
import com.example.tollsbyny.model.alpr.AlprResult;

public class SendPhotoToAlprAsync extends AsyncTask<Uri, Void, AlprResult> {

    private ApplicationActivity activity;

    public SendPhotoToAlprAsync(ApplicationActivity activity) {
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        activity.showProgress("Photo is being processed...");
    }

    @Override
    protected void onPostExecute(AlprResult alprResult) {
        super.onPostExecute(alprResult);

        Bundle b = new Bundle();
        b.putSerializable("alpr_result", alprResult);
        activity.initFragment(AddVehicleResult.class, false, b);
        activity.dismissDialog();
    }

    @Override
    protected AlprResult doInBackground(Uri... uris) {

        AlprResult result = new AlprClient().recognizeBytes(uris[0]);
        result.setUri(uris[0].toString());

        return result;
    }
}
