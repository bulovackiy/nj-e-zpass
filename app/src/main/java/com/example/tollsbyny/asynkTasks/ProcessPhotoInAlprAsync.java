package com.example.tollsbyny.asynkTasks;

import android.net.Uri;
import android.os.AsyncTask;

import com.example.tollsbyny.activity.ApplicationActivity;
import com.example.tollsbyny.client.AlprClient;
import com.example.tollsbyny.fragment.NewAddVehicleFragment;
import com.example.tollsbyny.model.alpr.AlprResult;

public class ProcessPhotoInAlprAsync extends AsyncTask<Uri, Void, AlprResult> {

    private NewAddVehicleFragment fragment;

    public ProcessPhotoInAlprAsync(NewAddVehicleFragment fragment) {
        this.fragment = fragment;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((ApplicationActivity) fragment.getActivity()).showProgress("Photo is being processed...");
    }

    @Override
    protected void onPostExecute(AlprResult alprResult) {
        super.onPostExecute(alprResult);
        fragment.initResults(alprResult);
        ((ApplicationActivity) fragment.getActivity()).dismissDialog();
    }

    @Override
    protected AlprResult doInBackground(Uri... uris) {
        AlprResult result = new AlprClient().recognizeBytes(uris[0]);
        result.setUri(uris[0].toString());

        return result;
    }
}
