package com.example.tollsbyny.asynkTasks;

import android.os.AsyncTask;

import com.example.tollsbyny.client.UserClient;
import com.example.tollsbyny.fragment.AddVehicleResult;
import com.example.tollsbyny.fragment.NewAddVehicleFragment;
import com.example.tollsbyny.model.lookup.VehicleModelInfo;

import java.util.List;

public class InitVehicleModelSpinnerAsync extends AsyncTask<String, Void, List<VehicleModelInfo>> {

    private NewAddVehicleFragment fragment;

    public InitVehicleModelSpinnerAsync(NewAddVehicleFragment fragment) {
        this.fragment = fragment;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        fragment.showVehicleModelProgressBar();
    }

    @Override
    protected void onPostExecute(List<VehicleModelInfo> vehicleModelInfos) {
        super.onPostExecute(vehicleModelInfos);
        fragment.initModelSpinner(vehicleModelInfos);
        fragment.dissmissVehicleModelProgressBar();
    }

    @Override
    protected List<VehicleModelInfo> doInBackground(String... strings) {
        UserClient client = new UserClient();

        return client.getVehicleModels(strings[0], strings[1]);
    }
}
