package com.example.tollsbyny.asynkTasks;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.tollsbyny.activity.ApplicationActivity;
import com.example.tollsbyny.client.AuthClient;
import com.example.tollsbyny.fragment.AccountOverviewFragment;
import com.example.tollsbyny.model.AccountDetailsModel;

import java.lang.ref.WeakReference;

public class InitAccountOverviewFragmentAsync extends AsyncTask<String, Void, AccountDetailsModel> {

    private WeakReference<ApplicationActivity> activity;

    public InitAccountOverviewFragmentAsync(ApplicationActivity activity) {
        this.activity = new WeakReference<>(activity);
    }

    @Override
    protected void onPreExecute() {
        activity.get().showProgress( "Getting account details...");
    }

    @Override
    protected void onPostExecute(AccountDetailsModel accountDetails) {
        super.onPostExecute(accountDetails);
        if (accountDetails == null) {
            new AlertDialog.Builder(activity.get())
                    .setTitle("Couldn't get account overview!!!")
                    .setMessage("")
                    .show();
        } else {
            Bundle b = new Bundle();
            b.putSerializable("account_details", accountDetails);
            activity.get().initFragment(AccountOverviewFragment.class, false, b);
        }
        activity.get().dismissDialog();
    }

    @Override
    protected AccountDetailsModel doInBackground(String... strings) {

        AuthClient client = new AuthClient();

        return client.getAccountDetails(strings[0]);
    }
}
