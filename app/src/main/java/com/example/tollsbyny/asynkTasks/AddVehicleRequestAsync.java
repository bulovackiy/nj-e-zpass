package com.example.tollsbyny.asynkTasks;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;

import com.example.tollsbyny.R;
import com.example.tollsbyny.activity.ApplicationActivity;

import java.util.concurrent.TimeUnit;

public class AddVehicleRequestAsync extends AsyncTask<String, Void, String> {

    private ApplicationActivity activity;

    public AddVehicleRequestAsync(ApplicationActivity activity) {
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        activity.showProgress("Vehicle is adding...");
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Uri soundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(activity, "NJ E-ZPASS 1")
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("Add Vehicle")
                .setContentText("Your vehicle has been successfully added")
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setSound(soundUri)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(false);

        NotificationManager notificationManager =(NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
        activity.dismissDialog();
    }

    @Override
    protected String doInBackground(String... strings) {

        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }
}
