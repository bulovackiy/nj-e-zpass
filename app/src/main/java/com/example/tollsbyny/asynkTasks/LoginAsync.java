package com.example.tollsbyny.asynkTasks;

import android.accounts.Account;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.example.tollsbyny.client.AuthClient;
import com.example.tollsbyny.fragment.LoginFragment;
import com.example.tollsbyny.model.AuthModel;
import com.example.tollsbyny.security.account.NjEzPassAccount;


public class LoginAsync extends AsyncTask<String, Void, AuthModel> {

    private LoginFragment fragment;
    private ProgressDialog dialog;

    public LoginAsync(LoginFragment fragment) {
        this.fragment = fragment;
    }

    @Override
    protected void onPreExecute() {
        dialog = ProgressDialog.show(fragment.getContext(), "Logging in...", "", true);
    }

    @Override
    protected void onPostExecute(AuthModel model) {
        if (model != null && model.getAccessToken() != null) {
            fragment.onAuthSuccess(new NjEzPassAccount(model.getValue()), model.getPassword(), model);
            dialog.dismiss();
        } else {
            dialog.dismiss();
            fragment.showError();
        }
    }

    @Override
    protected AuthModel doInBackground(String... strings) {

        AuthClient client = new AuthClient();

        return client.authenticate(strings[0], strings[1]);
    }
}
