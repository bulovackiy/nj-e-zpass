package com.example.tollsbyny.asynkTasks;

import android.os.AsyncTask;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import okhttp3.Response;

public class AsyncObjectMapper extends AsyncTask<Response, Void, JsonNode> {

    @Override
    protected JsonNode doInBackground(Response... responses) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(responses[0].body().bytes());
        } catch (IOException e) {
            return null;
        }
    }
}
