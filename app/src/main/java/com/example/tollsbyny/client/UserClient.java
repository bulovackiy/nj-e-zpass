package com.example.tollsbyny.client;

import android.content.res.Resources;

import com.example.tollsbyny.R;
import com.example.tollsbyny.ToolsByNyApp;
import com.example.tollsbyny.model.AccountDetailsModel;
import com.example.tollsbyny.model.lookup.VehicleMake;
import com.example.tollsbyny.model.lookup.VehicleModelInfo;
import com.example.tollsbyny.utils.JSONHelper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class UserClient {

    public List<VehicleMake> getVehicleMakes(String token) {
        Resources res = ToolsByNyApp.getContext().getResources();

        String getVehicleMakesUrl = res.getString(R.string.get_vehicle_makes_url);
        String agencyId = res.getString(R.string.oauth2_agency_id);
        String authorizationTokenName = res.getString(R.string.authorization_header_name);

        Request request = new Request.Builder()
                .url(getVehicleMakesUrl + "?agencyId=" + agencyId)
                .addHeader(authorizationTokenName, "Bearer " + token)
                .build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(180, TimeUnit.SECONDS)
                .connectionPool(new ConnectionPool(0, 1, TimeUnit.SECONDS))
                .readTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS)
                .build();

        List<VehicleMake> vehicleMakes = null;
        try {
            Response response = client.newCall(request).execute();
            vehicleMakes = Arrays.asList(JSONHelper.fromJson(response.body().string(), VehicleMake[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return vehicleMakes;
    }

    public List<VehicleModelInfo> getVehicleModels(String token, String make) {
        Resources res = ToolsByNyApp.getContext().getResources();

        String getVehicleMakesUrl = res.getString(R.string.get_vehicle_models_url).replace("{make}", make);
        String agencyId = res.getString(R.string.oauth2_agency_id);
        String authorizationTokenName = res.getString(R.string.authorization_header_name);

        Request request = new Request.Builder()
                .url(getVehicleMakesUrl + "?agencyId=" + agencyId)
                .addHeader(authorizationTokenName, "Bearer " + token)
                .build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(180, TimeUnit.SECONDS)
                .connectionPool(new ConnectionPool(0, 1, TimeUnit.SECONDS))
                .readTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS)
                .build();

        List<VehicleModelInfo> vehicleMakes = null;
        try {
            Response response = client.newCall(request).execute();
            vehicleMakes = Arrays.asList(JSONHelper.fromJson(response.body().string(), VehicleModelInfo[].class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return vehicleMakes;
    }
}
