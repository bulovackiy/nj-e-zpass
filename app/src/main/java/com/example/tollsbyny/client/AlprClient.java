package com.example.tollsbyny.client;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;

import com.example.tollsbyny.R;
import com.example.tollsbyny.ToolsByNyApp;
import com.example.tollsbyny.model.alpr.AlprResult;
import com.example.tollsbyny.utils.JSONHelper;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionPool;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class AlprClient {

    public AlprResult recognizeBytes(Uri uri) {
        try {
            String secret_key = ToolsByNyApp.getContext().getResources().getString(R.string.secret_alpr);

            // Read image file to byte array
            byte[] data = readBytes(uri);

            // Encode file bytes to base64
            byte[] encoded = Base64.getEncoder().encode(data);

            // Setup the HTTPS connection to api.openalpr.com
            URL url = new URL("https://api.openalpr.com/v2/recognize_bytes?recognize_vehicle=1&country=us&secret_key=" + secret_key);
            URLConnection con = url.openConnection();
            HttpURLConnection http = (HttpURLConnection) con;
            http.setRequestMethod("POST"); // PUT is another valid option
            http.setFixedLengthStreamingMode(encoded.length);
            http.setDoOutput(true);

            // Send our Base64 content over the stream
            try (OutputStream os = http.getOutputStream()) {
                os.write(encoded);
            }

            int status_code = http.getResponseCode();
            if (status_code == 200) {
                // Read the response
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        http.getInputStream()));
                String json_content = "";
                String inputLine;
                while ((inputLine = in.readLine()) != null)
                    json_content += inputLine;
                in.close();

                AlprResult result = JSONHelper.fromJson(json_content, AlprResult.class);

                //result.setJson(json_content);

                return result;
            } else {
                return null;
            }

        } catch (MalformedURLException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    private byte[] readBytes(Uri uri) {
        try {
            Bitmap bitmap = getBitmap(uri);
            OutputStream outputStream = ToolsByNyApp.getContext().getContentResolver().openOutputStream(uri);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            InputStream is = ToolsByNyApp.getContext().getContentResolver().openInputStream(uri);
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int nRead;
            byte[] data = new byte[1024];
            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();

            return  buffer.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private Bitmap getBitmap(Uri uri) throws FileNotFoundException {
        Bitmap bitmap = BitmapFactory.decodeStream(ToolsByNyApp.getContext().getContentResolver().openInputStream(uri));

        int h = bitmap.getHeight();
        int w = bitmap.getWidth();

        double k = ((h/1280d) + (w/760d))/2;

        bitmap = Bitmap.createScaledBitmap(bitmap, (int) (w / k), (int) (h / k), true);
        return bitmap;
    }
}
