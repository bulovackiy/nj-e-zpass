package com.example.tollsbyny.client;

import android.content.res.Resources;
import android.util.Log;

import com.example.tollsbyny.R;
import com.example.tollsbyny.ToolsByNyApp;
import com.example.tollsbyny.constants.Header;
import com.example.tollsbyny.model.AccountDetailsModel;
import com.example.tollsbyny.model.AuthModel;
import com.example.tollsbyny.utils.JSONHelper;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionPool;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AuthClient {

    public AuthModel authenticate(String userId, String password) {
        Resources res = ToolsByNyApp.getContext().getResources();

        String authUrl = res.getString(R.string.oauth2_token_url);
        String grantType = res.getString(R.string.oauth2_grant_type);
        String clientId = res.getString(R.string.oauth2_client_id);
        String clientSecret = res.getString(R.string.oauth2_client_secret);
        String agencyId = res.getString(R.string.oauth2_agency_id);

        HttpUrl url = HttpUrl.parse(authUrl)
                .newBuilder()
                .addQueryParameter(Header.GRANT_TYPE.getValue(), grantType)
                .addQueryParameter(Header.CLIENT_ID.getValue(), clientId)
                .addQueryParameter(Header.CLIENT_SECRET.getValue(), clientSecret)
                .addQueryParameter(Header.AGENCY_ID.getValue(), agencyId)
                .addQueryParameter(Header.VALUE.getValue(), userId)
                .addQueryParameter(Header.PASSWORD.getValue(), password)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(null, new byte[]{}))
                .build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(180, TimeUnit.SECONDS)
                .connectionPool(new ConnectionPool(0, 1, TimeUnit.SECONDS))
                .readTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS)
                .build();

        AuthModel model = null;

        try {
            Response response = client.newCall(request).execute();
            model = JSONHelper.fromJson(response.body().string(), AuthModel.class);
            model.setValue(userId);
            model.setPassword(password);
        } catch (IOException e) {
            Log.e("AuthClient", "User isn't authenticated", e);
        }

        return model;
    }

    public AuthModel refreshToken (String refreshToken) {
        Resources res = ToolsByNyApp.getContext().getResources();

        String authUrl = res.getString(R.string.oauth2_token_url);
        String grantType = res.getString(R.string.oauth2_refresh_grant_type);
        String clientId = res.getString(R.string.oauth2_client_id);
        String clientSecret = res.getString(R.string.oauth2_client_secret);

        HttpUrl url = HttpUrl.parse(authUrl)
                .newBuilder()
                .addQueryParameter(Header.GRANT_TYPE.getValue(), grantType)
                .addQueryParameter(Header.REFRESH_TOKEN.getValue(), refreshToken)
                .addQueryParameter(Header.CLIENT_ID.getValue(), clientId)
                .addQueryParameter(Header.CLIENT_SECRET.getValue(), clientSecret)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(null, new byte[]{}))
                .build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(180, TimeUnit.SECONDS)
                .connectionPool(new ConnectionPool(0, 1, TimeUnit.SECONDS))
                .readTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS)
                .build();

        AuthModel model = null;
        try {
            Response response = client.newCall(request).execute();
            model = JSONHelper.fromJson(response.body().string(), AuthModel.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return model;
    }

    public AccountDetailsModel getAccountDetails(String token) {
        Resources res = ToolsByNyApp.getContext().getResources();

        String getAccountDetailsUrl = res.getString(R.string.get_account_details_url);
        String authorizationTokenName = res.getString(R.string.authorization_header_name);

        Request request = new Request.Builder()
                .url(getAccountDetailsUrl)
                .addHeader(authorizationTokenName, "Bearer " + token)
                .build();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(180, TimeUnit.SECONDS)
                .connectionPool(new ConnectionPool(0, 1, TimeUnit.SECONDS))
                .readTimeout(180, TimeUnit.SECONDS)
                .writeTimeout(180, TimeUnit.SECONDS)
                .build();

        AccountDetailsModel model = null;
        try {
            Response response = client.newCall(request).execute();
            model = JSONHelper.fromJson(response.body().string(), AccountDetailsModel.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return model;
    }
}
