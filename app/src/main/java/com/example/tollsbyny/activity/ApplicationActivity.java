package com.example.tollsbyny.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.tollsbyny.R;
import com.example.tollsbyny.ToolsByNyApp;
import com.example.tollsbyny.asynkTasks.InitAccountOverviewFragmentAsync;
import com.example.tollsbyny.fragment.NewAddVehicleFragment;
import com.example.tollsbyny.security.service.AuthService;


public class ApplicationActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public ProgressDialog dialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.transactions_home);

        if (ContextCompat.checkSelfPermission(ToolsByNyApp.getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
        }

        initToolBar();
    }

    private void initToolBar() {
        Toolbar toolbar = findViewById(R.id.ezpass_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorWhite)));

        DrawerLayout drawer = findViewById(R.id.ezpass_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        dialog = new ProgressDialog(this);

        final NavigationView navigationView = findViewById(R.id.nav_view_application);
        navigationView.setNavigationItemSelectedListener(this);

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                Fragment fragment = getCurrentFragment();
                int id = fragment.getView().getId();

                switch (id) {
                    case R.id.account_overview_fragment:
                        navigationView.setCheckedItem(R.id.account_details_window);
                        break;
                    case R.id.add_vehicle_layout:
                        navigationView.setCheckedItem(R.id.add_vehicle_window);
                        break;
                }
            }
        });
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();

        if (AuthService.isAccountExists()) {
            initAccountOverviewFragment();
        } else {
            Intent i = new Intent(this, LoginActivity.class);
            startActivityForResult(i, 111);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 111) {
            initAccountOverviewFragment();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();

        switch (id) {
            case R.id.account_details_window:
                initAccountOverviewFragment();
                menuItem.setChecked(true);
                break;
            case R.id.add_vehicle_window:
                initFragment(NewAddVehicleFragment.class, true, null);
                menuItem.setChecked(true);
                break;
            case R.id.log_out:
                logout();
                break;
        }

        DrawerLayout drawer = findViewById(R.id.ezpass_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    public void showProgress(String message) {
        dialog.dismiss();
        dialog.setTitle(message);
        dialog.show();
    }

    public void dismissDialog() {
        dialog.dismiss();
    }

    public void initFragment(Class<? extends Fragment> fragmentClass, boolean addToBackStack, Bundle b) {
        initFragment(fragmentClass, addToBackStack, b,true);
    }

    private void initFragment(Class<? extends Fragment> fragmentClass, boolean addToBackStack, Bundle b, boolean commit) {
        Fragment fragment = null;
        try {
            fragment = fragmentClass.newInstance();

            if (b != null)
                fragment.setArguments(b);

        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);

        if (addToBackStack)
            ft.addToBackStack(null);

        if (commit)
            ft.commit();
    }

    private Fragment getCurrentFragment() {
        return this.getSupportFragmentManager().findFragmentById(R.id.container);
    }

    private void logout() {
        AuthService.clearAccountManager();

        Intent intent = new Intent(this, LoginActivity.class);
        this.startActivityForResult(intent, 111);

        clearFragments();
    }

    public void clearFragments() {
        getSupportFragmentManager()
                .beginTransaction()
                .remove(getCurrentFragment())
                .commit();
    }

    private void initAccountOverviewFragment() {
        new InitAccountOverviewFragmentAsync(this).execute(AuthService.getAuthToken());
    }
}
