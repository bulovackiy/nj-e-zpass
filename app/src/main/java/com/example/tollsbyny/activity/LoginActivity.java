package com.example.tollsbyny.activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


import com.example.tollsbyny.R;
import com.example.tollsbyny.fragment.LoginFragment;

public class LoginActivity extends AppCompatActivity {

    public static final String EXTRA_TOKEN_TYPE = "com.example.tollsbyny.EXTRA_TOKEN_TYPE";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_home);

        Toolbar toolbar = findViewById(R.id.ezpass_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorWhite)));

        DrawerLayout drawer = findViewById(R.id.login_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NotificationChannel channel = new NotificationChannel("NJ E-ZPASS 1", "NJ E-ZPASS notifier", NotificationManager.IMPORTANCE_HIGH);
        channel.setDescription("NJ E-ZPASS");

        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new LoginFragment())
                .commit();
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}
