package com.example.tollsbyny.constants;

public enum Preference {

    ACCESS_TOKEN("access_token");

    private String value;

    Preference(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
