package com.example.tollsbyny.constants;

public enum Header {

    GRANT_TYPE("grant_type"),
    CLIENT_ID("client_id"),
    CLIENT_SECRET("client_secret"),
    AGENCY_ID("agencyID"),
    LOGIN_TYPE("loginType"),
    VALUE("value"),
    REFRESH_TOKEN("refresh_token"),
    PASSWORD("password");

    private String value;

    Header(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
