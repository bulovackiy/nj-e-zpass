package com.example.tollsbyny.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.tollsbyny.R;
import com.example.tollsbyny.model.AccountDetailsModel;
import com.example.tollsbyny.model.info.AccountInfoModel;
import com.example.tollsbyny.model.info.FinancialInfoModel;
import com.example.tollsbyny.model.info.PersonalInfoModel;
import com.example.tollsbyny.model.info.ReplenishmentInfoModel;
import com.example.tollsbyny.utils.PriceUtil;

public class AccountOverviewFragment extends Fragment {

    private TextView nameField;
    private TextView accountNumberField;
    private TextView prepaidTollBalanceField;
    private TextView violationBalanceField;
    private TextView lastReplenishedField;
    private TextView replenishAmountField;
    private TextView replenishThresholdField;
    private TextView replenishTypeField;
    private ScrollView mainScroll;
    private ScrollView childScrool;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.account_overview_fragment, container, false);

        AccountDetailsModel model = (AccountDetailsModel) getArguments().getSerializable("account_details");

        nameField = view.findViewById(R.id.account_overview_name);
        accountNumberField = view.findViewById(R.id.account_overview_account_number);
        prepaidTollBalanceField = view.findViewById(R.id.account_overview_prepaid_toll_balance);
        violationBalanceField = view.findViewById(R.id.account_overview_violations_balance);
        lastReplenishedField = view.findViewById(R.id.account_overview_last_replenished);
        replenishAmountField = view.findViewById(R.id.account_overview_replenish_amount);
        replenishThresholdField = view.findViewById(R.id.account_overview_replenish_amount_update);
        //replenishTypeField = view.findViewById(R.id.replenish_type);

        if (model != null) {

            PersonalInfoModel personal = model.getPersonalInformation();
            AccountInfoModel accountInfo = model.getAccountInformation();
            FinancialInfoModel financialInfo = model.getFinancialInformation();
            ReplenishmentInfoModel replenishmentInfoModel = model.getReplenishmentInformation();

            if (personal != null) {
                String name = personal.getFirstName() + " " + personal.getLastName();

                nameField.setText(name);
            }

            if (accountInfo != null) {
                String accountNumber = accountInfo.getNumber();
                String violationBalance = "$" + PriceUtil.convertIntegerToPrice(accountInfo.getOpenViolationCount());

                accountNumberField.setText(accountNumber);
                violationBalanceField.setText(violationBalance);
            }

            if (financialInfo != null) {
                String prepaidTollBalance = "$" + PriceUtil.convertBigDecimalToPrice(financialInfo.getCurrentBalance());

                prepaidTollBalanceField.setText(prepaidTollBalance);
            }

            if (replenishmentInfoModel != null) {
                String replenishAmount = "$" + PriceUtil.convertBigDecimalToPrice(replenishmentInfoModel.getLastReplenishedAmount());
                String replenishThreshold = "$" + PriceUtil.convertBigDecimalToPrice(replenishmentInfoModel.getAutomaticReplenishmentThreshold());

                replenishThresholdField.setText(replenishThreshold);
                //replenishTypeField.setText(replenishmentInfoModel.getType());
                lastReplenishedField.setText(replenishmentInfoModel.getLastReplenishedDate());
                replenishAmountField.setText(replenishAmount);
            }
        }

        mainScroll = view.findViewById(R.id.account_overview_fragment);
        childScrool = view.findViewById(R.id.transactions_scroll);

        mainScroll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                view.findViewById(R.id.transactions_scroll).getParent().requestDisallowInterceptTouchEvent(false);

                return false;
            }
        });

        childScrool.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        return view;
    }
}
