package com.example.tollsbyny.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.tollsbyny.R;
import com.example.tollsbyny.activity.ApplicationActivity;
import com.example.tollsbyny.asynkTasks.AddVehicleRequestAsync;
import com.example.tollsbyny.asynkTasks.InitAccountOverviewFragmentAsync;
import com.example.tollsbyny.model.alpr.AlprInfo;
import com.example.tollsbyny.model.alpr.AlprResult;
import com.example.tollsbyny.model.alpr.Result;
import com.example.tollsbyny.model.lookup.VehicleMake;
import com.example.tollsbyny.model.lookup.VehicleModelInfo;
import com.example.tollsbyny.utils.DefaultPref;

import java.util.ArrayList;
import java.util.List;

public class AddVehicleResult extends Fragment {

    private ImageView vehiclePhoto;
    private EditText plateNumber;
    private EditText year;
    private Spinner make;
    private Spinner model;
    private Spinner state;
    private ProgressBar makeProgressBar;
    private ProgressBar modelProgressBar;
    private TextView jsonResult;
    private Button addButton;
    private Button cancelButton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_vehicle_result_layout, container, false);

        AlprResult alprResult = (AlprResult) getArguments().getSerializable("alpr_result");

       // jsonResult = view.findViewById(R.id.json_resut);
        vehiclePhoto = view.findViewById(R.id.vehicle_photo);
        plateNumber = view.findViewById(R.id.plate_number);
        make = view.findViewById(R.id.make);
        model = view.findViewById(R.id.model);
        makeProgressBar = view.findViewById(R.id.make_progress_bar);
        modelProgressBar = view.findViewById(R.id.model_progress_bar);
        state = view.findViewById(R.id.state);
        year = view.findViewById(R.id.year);
        addButton = view.findViewById(R.id.add_vehicle_button);
        cancelButton = view.findViewById(R.id.cancel_add_vehicle_button);

        //new InitVehicleMakesAsync(this).execute(DefaultPref.getAccessToken());


        if (alprResult != null && alprResult.getResults().size() > 0) {
            Result result = alprResult.getResults().get(0);
            setState(result.getRegion());
            plateNumber.setText(result.getPlate());
        }

        if (alprResult != null &&
                alprResult.getUri() != null && !alprResult.getUri().isEmpty()) {
            //jsonResult.setText(alprResult.getJson());
            vehiclePhoto.setImageURI(Uri.parse(alprResult.getUri()));
        }

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AddVehicleRequestAsync((ApplicationActivity) getActivity()).execute();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new InitAccountOverviewFragmentAsync((ApplicationActivity) getActivity()).execute(DefaultPref.getAccessToken());
            }
        });

        return view;
    }

    private void setState(String stateName) {
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(getContext(), R.array.states, R.layout.spinner_item);
        state.setSelection(arrayAdapter.getPosition(stateName.toUpperCase()));
    }

    public void initMakeSpinner(List<VehicleMake> makes) {
        List<String> makesNames = new ArrayList<>();

        for (VehicleMake vehicleMake : makes) {
            makesNames.add(vehicleMake.getName());
        }

        AlprResult alprResult = (AlprResult) getArguments().getSerializable("alpr_result");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this.getContext(), R.layout.spinner_item, makesNames);
        make.setAdapter(arrayAdapter);

        if (alprResult != null && alprResult.getResults().size() > 0) {
            Result result = alprResult.getResults().get(0);
            AlprInfo alprMakeModel = result.getVehicle().getMakeModel().get(0);
            String vehicleMake = alprMakeModel.getName().split("_")[0].toUpperCase();
            setYear(result);
            int position = arrayAdapter.getPosition(vehicleMake);

            if (position == 0)
                position = arrayAdapter.getPosition("OTHER");

            make.setSelection(position);
            init(vehicleMake);
        }
        make.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                init(arrayAdapter.getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setYear(Result result) {
        String resultYear = result.getVehicle().getYear().get(0).getName();

        if (resultYear.contains("-"))
            year.setText(result.getVehicle().getYear().get(0).getName().split("-")[1]);
        else
            year.setText(resultYear);
    }

    public void init(String vehicleMake) {
        //new InitVehicleModelSpinnerAsync(this).execute(DefaultPref.getAccessToken(), vehicleMake);
    }

    public void initModelSpinner(List<VehicleModelInfo> vehicleModelInfo) {
        List<String> models = new ArrayList<>();

        for (VehicleModelInfo modelInfo : vehicleModelInfo) {
            models.add(modelInfo.getName());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this.getContext(), R.layout.spinner_item, models);
        model.setAdapter(arrayAdapter);

        AlprResult alprResult = (AlprResult) getArguments().getSerializable("alpr_result");

        if (alprResult != null && alprResult.getResults().size() > 0) {
            Result result = alprResult.getResults().get(0);
            AlprInfo alprMakeModel = result.getVehicle().getMakeModel().get(0);
            String vehicleModel = alprMakeModel.getName().split("_")[1].toUpperCase();

            int position = arrayAdapter.getPosition(vehicleModel);

            if (position == 0)
                position = arrayAdapter.getPosition("OTHER");

            model.setSelection(position);
        }
    }

    public void showVehicleMakeProgressBar() {
        makeProgressBar.setVisibility(View.VISIBLE);
    }

    public void dissmissVehicleMakeProgressBar() {
        makeProgressBar.setVisibility(View.INVISIBLE);
    }

    public void showVehicleModelProgressBar() {
        modelProgressBar.setVisibility(View.VISIBLE);
    }

    public void dissmissVehicleModelProgressBar() {
        modelProgressBar.setVisibility(View.INVISIBLE);
    }


}
