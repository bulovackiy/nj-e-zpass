package com.example.tollsbyny.fragment;

import android.Manifest;
import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.tollsbyny.R;
import com.example.tollsbyny.ToolsByNyApp;
import com.example.tollsbyny.activity.ApplicationActivity;
import com.example.tollsbyny.asynkTasks.SendPhotoToAlprAsync;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.Inflater;

import static android.app.Activity.RESULT_OK;

public class AddVehicleFragment extends Fragment {

    private Button continueButton;
    private Uri file;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_vehicle_layout, container, false);

        continueButton = view.findViewById(R.id.add_vehicle_continue);

        if (ContextCompat.checkSelfPermission(ToolsByNyApp.getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            continueButton.setEnabled(false);
            ActivityCompat.requestPermissions(this.getActivity(), new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                file = FileProvider.getUriForFile(ToolsByNyApp.getContext(), "com.example.tollsbyny.provider", getOutputMediaFile());
                intent.putExtra("outputFormat",
                        Bitmap.CompressFormat.JPEG.toString());
                intent.putExtra(MediaStore.EXTRA_OUTPUT, file);

                startActivityForResult(intent, 100);
            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                continueButton.setEnabled(true);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                new SendPhotoToAlprAsync((ApplicationActivity) getActivity()).execute(file);
            }
        }
    }

    private File getOutputMediaFile(){
        File mediaStorageDir = new File(getActivity().getExternalFilesDir(
                Environment.DIRECTORY_PICTURES), "Vehicles");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");
    }
}
