package com.example.tollsbyny.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.tollsbyny.R;
import com.example.tollsbyny.ToolsByNyApp;
import com.example.tollsbyny.activity.ApplicationActivity;
import com.example.tollsbyny.asynkTasks.AddVehicleRequestAsync;
import com.example.tollsbyny.asynkTasks.InitVehicleMakesAsync;
import com.example.tollsbyny.asynkTasks.InitVehicleModelSpinnerAsync;
import com.example.tollsbyny.asynkTasks.ProcessPhotoInAlprAsync;
import com.example.tollsbyny.model.alpr.AlprResult;
import com.example.tollsbyny.model.alpr.Result;
import com.example.tollsbyny.model.lookup.VehicleMake;
import com.example.tollsbyny.model.lookup.VehicleModelInfo;
import com.example.tollsbyny.security.service.AuthService;
import com.example.tollsbyny.utils.DefaultPref;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class NewAddVehicleFragment extends Fragment {

    private final String OTHER = "OTHER";
    private String vehicleModel = null;

    private ImageView addPhotoArea;
    private Uri file;
    private TextInputEditText addVehiclePlateNumber;
    private TextInputEditText addVehicleYear;
    private AppCompatSpinner addVehicleCountrySpinner;
    private AppCompatSpinner addVehicleStateSpinner;
    private AppCompatSpinner addVehicleMakeSpinner;
    private AppCompatSpinner addVehicleModelSpinner;
    private Button continueButton;
    private Button cancelButton;
    private ProgressBar makeProgressBar;
    private ProgressBar modelProgressBar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_vehicle_fragment, container, false);

        addPhotoArea = v.findViewById(R.id.add_vehicle_photo_area);
        addVehiclePlateNumber = v.findViewById(R.id.add_vehicle_plate_number);
        addVehicleYear = v.findViewById(R.id.add_vehicle_year);
        addVehicleCountrySpinner = v.findViewById(R.id.add_vehicle_country_spinner);
        addVehicleStateSpinner = v.findViewById(R.id.add_vehicle_state_spinner);
        addVehicleMakeSpinner = v.findViewById(R.id.add_vehicle_make_spinner);
        addVehicleModelSpinner = v.findViewById(R.id.add_vehicle_model_spinner);
        continueButton = v.findViewById(R.id.add_vehicle_continue_button);
        cancelButton = v.findViewById(R.id.add_vehicle_cancel_button);
        makeProgressBar = v.findViewById(R.id.add_vehicle_make_progress_bar);
        modelProgressBar = v.findViewById(R.id.add_vehicle_model_progress_bar);

        initAddPhotoArea();
        initContinueButton();
        initCancelButton();
        new InitVehicleMakesAsync(this).execute(AuthService.getAuthToken());

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                new ProcessPhotoInAlprAsync(this).execute(file);
            }
        }
    }


    public void initResults(AlprResult result) {

        if (result != null && result.getResults().size() > 0) {
            Result r = result.getResults().get(0);
            setState(r.getRegion());
            addVehiclePlateNumber.setText(r.getPlate());
            String[] makeModel = r.getVehicle().getMakeModel().get(0).getName().split("_");
            setMake(makeModel[0]);
            vehicleModel = makeModel[1];
            setYear(r.getVehicle().getYear().get(0).getName());
        }

        if (file != null)
            addPhotoArea.setImageURI(file);
    }

    public void showVehicleMakeProgressBar() {
        makeProgressBar.setVisibility(View.VISIBLE);
    }

    public void dissmissVehicleMakeProgressBar() {
        makeProgressBar.setVisibility(View.INVISIBLE);
    }

    public void showVehicleModelProgressBar() {
        modelProgressBar.setVisibility(View.VISIBLE);
    }

    public void dissmissVehicleModelProgressBar() {
        modelProgressBar.setVisibility(View.INVISIBLE);
    }

    public void initMakeSpinner(List<VehicleMake> makes) {
        List<String> makesNames = new ArrayList<>();

        for (VehicleMake vehicleMake : makes) {
            makesNames.add(vehicleMake.getName());
        }

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this.getContext(), R.layout.spinner_item, makesNames);
        addVehicleMakeSpinner.setAdapter(arrayAdapter);

        addVehicleMakeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setModel(arrayAdapter.getItem(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setModel(String vehicleMake) {
        new InitVehicleModelSpinnerAsync(this).execute(AuthService.getAuthToken(), vehicleMake);
    }

    public void setMake(String vehicleMake) {
        ArrayAdapter adapter = (ArrayAdapter) addVehicleMakeSpinner.getAdapter();
        int position = adapter.getPosition(vehicleMake.toUpperCase());

        addVehicleMakeSpinner.setSelection(position);
    }

    private void setYear(String years) {
        if (years.contains("-"))
            addVehicleYear.setText(years.split("-")[1]);
        else
            addVehicleYear.setText(years);
    }

    public void initModelSpinner(List<VehicleModelInfo> vehicleModelInfo) {
        List<String> models = new ArrayList<>();

        if (vehicleModelInfo != null && vehicleModelInfo.size() != 0) {
            for (VehicleModelInfo modelInfo : vehicleModelInfo) {
                models.add(modelInfo.getName());
            }
        } else {
            models.add(OTHER);
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this.getContext(), R.layout.spinner_item, models);
        addVehicleModelSpinner.setAdapter(arrayAdapter);

        if (vehicleModel != null && !vehicleModel.isEmpty()) {
            int position = arrayAdapter.getPosition(vehicleModel.toUpperCase());

            if (position == -1)
                position = arrayAdapter.getPosition(OTHER);

            addVehicleModelSpinner.setSelection(position);

            vehicleModel = null;
        }
    }

    private void initContinueButton() {
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AddVehicleRequestAsync((ApplicationActivity) getActivity()).execute();
            }
        });

    }

    private void initCancelButton() {
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });

    }

    private void setState(String stateName) {
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(getContext(), R.array.states, R.layout.spinner_item);
        addVehicleStateSpinner.setSelection(arrayAdapter.getPosition(stateName.toUpperCase()));
    }

    private void initAddPhotoArea() {
        addPhotoArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(ToolsByNyApp.getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                } else {
                    new AlertDialog.Builder(getContext())
                            .setTitle("Add Vehicle")
                            .setMessage("To proceed, take a picture of the back of the vehicle, including the license plate.")
                            .setNeutralButton("Continue", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    file = FileProvider.getUriForFile(ToolsByNyApp.getContext(), "com.example.tollsbyny.provider", getOutputMediaFile());
                                    intent.putExtra("outputFormat",
                                            Bitmap.CompressFormat.JPEG.toString());
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, file);

                                    startActivityForResult(intent, 100);
                                }
                            })
                            .show();
                }
            }
        });
    }


    private File getOutputMediaFile(){
        File mediaStorageDir = new File(getActivity().getExternalFilesDir(
                Environment.DIRECTORY_PICTURES), "Vehicles");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");
    }
}
