package com.example.tollsbyny.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.tollsbyny.R;
import com.example.tollsbyny.ToolsByNyApp;
import com.example.tollsbyny.asynkTasks.LoginAsync;
import com.example.tollsbyny.model.AuthModel;
import com.example.tollsbyny.security.account.NjEzPassAccount;
import com.example.tollsbyny.security.service.AuthService;

import static android.app.Activity.RESULT_OK;

public class LoginFragment extends Fragment implements View.OnClickListener {

    private Button loginButton;
    private EditText userId;
    private EditText password;
    private TextInputLayout userIdInputLayout;
    private TextInputLayout passwordInputLayout;
    private ImageView alertImage;
    private SwitchCompat loginRememberMe;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_layout_fragment, container, false);

        loginButton = v.findViewById(R.id.login_button);
        userId = v.findViewById(R.id.user_id_input);
        password = v.findViewById(R.id.password_input);
        userIdInputLayout = v.findViewById(R.id.user_id_input_layout);
        passwordInputLayout = v.findViewById(R.id.password_input_layout);
        alertImage = v.findViewById(R.id.alert_image);
        loginRememberMe = v.findViewById(R.id.login_remember_me);

        loginButton.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_button:
                login();
                break;
        }
    }

    public void showError() {
        userIdInputLayout.setError(" ");
        passwordInputLayout.setError(ToolsByNyApp.getContext().getResources().getString(R.string.authentication_error_message));
        alertImage.setVisibility(View.VISIBLE);
    }

    public void onAuthSuccess(NjEzPassAccount account, String password, AuthModel authModel) {
        AuthService.clearAccountManager();

        if (loginRememberMe.isChecked()) {
            AuthService.addAccount(account, password, authModel.getAccessToken(), authModel.getRefreshToken(), authModel.getExpiresIn());
        } else {
            AuthService.addToken(authModel.getAccessToken());
        }

        getActivity().setResult(RESULT_OK);
        getActivity().finish();
    }

    private void login() {

        new LoginAsync(this).execute(userId.getText().toString(), password.getText().toString());
    }

}