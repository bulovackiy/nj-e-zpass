package com.example.tollsbyny.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.tollsbyny.ToolsByNyApp;
import com.example.tollsbyny.constants.Preference;

public class DefaultPref {

    private static final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ToolsByNyApp.getContext());

    public static void setAccessToken(String value) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Preference.ACCESS_TOKEN.getValue(), value);
        editor.apply();
    }

    public static String getAccessToken() {
        return preferences.getString(Preference.ACCESS_TOKEN.getValue(), null);
    }
}
