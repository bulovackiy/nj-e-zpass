package com.example.tollsbyny.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JSONHelper {

    public static <T> String toJson(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }

    public static <T> T fromJson(String json, Class<T> object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, object);
    }
}