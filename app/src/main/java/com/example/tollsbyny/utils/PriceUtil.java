package com.example.tollsbyny.utils;

import java.math.BigDecimal;

public class PriceUtil {

    public static String convertBigDecimalToPrice(BigDecimal bigDecimal) {

        return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
    }

    public static String convertIntegerToPrice(Integer integer) {

        return integer + ".00";
    }
}
