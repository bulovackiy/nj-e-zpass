package com.example.tollsbyny.model.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonalInfoModel implements Serializable {

    private String userName, title, firstName, middleInitial, lastName, suffix, addressLine1, addressLine2,
            city, state, zipCode, zipCodePlus, country, daytimePhone, cellPhone, eveningPhone, fax,
            emailAddress, mobileAlerts, surveyOptIn;

    public String getTitle() {
        return title;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleInitial() {
        return middleInitial;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getZipCodePlus() {
        return zipCodePlus;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleInitial(String middleInitial) {
        this.middleInitial = middleInitial;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setZipCodePlus(String zipCodePlus) {
        this.zipCodePlus = zipCodePlus;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getMobileAlerts() {
        return mobileAlerts;
    }

    public void setMobileAlerts(String mobileAlerts) {
        this.mobileAlerts = mobileAlerts;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDaytimePhone() {
        return daytimePhone;
    }

    public void setDaytimePhone(String daytimePhone) {
        this.daytimePhone = daytimePhone;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getEveningPhone() {
        return eveningPhone;
    }

    public void setEveningPhone(String eveningPhone) {
        this.eveningPhone = eveningPhone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getSurveyOptIn() {
        return surveyOptIn;
    }

    public void setSurveyOptIn(String surveyOptIn) {
        this.surveyOptIn = surveyOptIn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonalInfoModel that = (PersonalInfoModel) o;
        return Objects.equals(userName, that.userName) &&
                Objects.equals(title, that.title) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(middleInitial, that.middleInitial) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(suffix, that.suffix) &&
                Objects.equals(addressLine1, that.addressLine1) &&
                Objects.equals(addressLine2, that.addressLine2) &&
                Objects.equals(country, that.country) &&
                Objects.equals(city, that.city) &&
                Objects.equals(state, that.state) &&
                Objects.equals(zipCode, that.zipCode) &&
                Objects.equals(zipCodePlus, that.zipCodePlus) &&
                Objects.equals(daytimePhone, that.daytimePhone) &&
                Objects.equals(cellPhone, that.cellPhone) &&
                Objects.equals(eveningPhone, that.eveningPhone) &&
                Objects.equals(fax, that.fax) &&
                Objects.equals(emailAddress, that.emailAddress) &&
                Objects.equals(mobileAlerts, that.mobileAlerts) &&
                Objects.equals(surveyOptIn, that.surveyOptIn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, title, firstName, middleInitial, lastName, suffix, addressLine1, addressLine2, country, city, state, zipCode, zipCodePlus, daytimePhone, cellPhone, eveningPhone, fax, emailAddress, mobileAlerts, surveyOptIn);
    }

    @Override
    public String toString() {
        return "PersonalInfoModel{" +
                "userName='" + userName + '\'' +
                ", title='" + title + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleInitial='" + middleInitial + '\'' +
                ", lastName='" + lastName + '\'' +
                ", suffix='" + suffix + '\'' +
                ", addressLine1='" + addressLine1 + '\'' +
                ", addressLine2='" + addressLine2 + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", zipCodePlus='" + zipCodePlus + '\'' +
                ", daytimePhone='" + daytimePhone + '\'' +
                ", cellPhone='" + cellPhone + '\'' +
                ", eveningPhone='" + eveningPhone + '\'' +
                ", fax='" + fax + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", mobileAlerts='" + mobileAlerts + '\'' +
                ", surveyOptIn=" + surveyOptIn +
                '}';
    }
}
