package com.example.tollsbyny.model.lookup;

public class VehicleModelInfo {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}