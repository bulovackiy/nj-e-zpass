package com.example.tollsbyny.model.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReplenishmentInfoModel implements Serializable {

    private String type;
    private BigDecimal automaticReplenishmentThreshold;
    private String lastReplenishedDate;
    private BigDecimal lastReplenishedAmount;

    public String getType() {
        return type;
    }

    public BigDecimal getAutomaticReplenishmentThreshold() {
        return automaticReplenishmentThreshold;
    }

    public String getLastReplenishedDate() {
        return lastReplenishedDate;
    }

    public BigDecimal getLastReplenishedAmount() {
        return lastReplenishedAmount;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAutomaticReplenishmentThreshold(BigDecimal automaticReplenishmentThreshold) {
        this.automaticReplenishmentThreshold = automaticReplenishmentThreshold;
    }

    public void setLastReplenishedDate(String lastReplenishedDate) {
        this.lastReplenishedDate = lastReplenishedDate;
    }

    public void setLastReplenishedAmount(BigDecimal lastReplenishedAmount) {
        this.lastReplenishedAmount = lastReplenishedAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReplenishmentInfoModel that = (ReplenishmentInfoModel) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(automaticReplenishmentThreshold, that.automaticReplenishmentThreshold) &&
                Objects.equals(lastReplenishedDate, that.lastReplenishedDate) &&
                Objects.equals(lastReplenishedAmount, that.lastReplenishedAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, automaticReplenishmentThreshold, lastReplenishedDate, lastReplenishedAmount);
    }
}
