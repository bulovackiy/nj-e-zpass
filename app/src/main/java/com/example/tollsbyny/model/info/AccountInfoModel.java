package com.example.tollsbyny.model.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountInfoModel implements Serializable {

    private String status;
    private String number;
    private String type;
    private Integer openViolationCount;
    private String challengeQuestion;
    private String challengeAnswer;
    private String password;

    //getters
    public String getNumber() {
        return number;
    }

    public String getType() {
        return type;
    }

    public Integer getOpenViolationCount() {
        return openViolationCount;
    }

    public String getChallengeQuestion() {
        return challengeQuestion;
    }

    public String getChallengeAnswer() {
        return challengeAnswer;
    }

    public String getPassword() {
        return password;
    }

    public String getStatus() {
        return status;
    }

    //setters
    public void setNumber(String number) {
        this.number = number;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setOpenViolationCount(Integer openViolationCount) {
        this.openViolationCount = openViolationCount;
    }

    public void setChallengeQuestion(String challengeQuestion) {
        this.challengeQuestion = challengeQuestion;
    }

    public void setChallengeAnswer(String challengeAnswer) {
        this.challengeAnswer = challengeAnswer;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountInfoModel that = (AccountInfoModel) o;
        return Objects.equals(status, that.status) &&
                Objects.equals(number, that.number) &&
                Objects.equals(type, that.type) &&
                Objects.equals(openViolationCount, that.openViolationCount) &&
                Objects.equals(challengeQuestion, that.challengeQuestion) &&
                Objects.equals(challengeAnswer, that.challengeAnswer) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, number, type, openViolationCount, challengeQuestion, challengeAnswer, password);
    }

    @Override
    public String toString() {
        return "AccountInfoModel{" +
                "status='" + status + '\'' +
                ", number='" + number + '\'' +
                ", type='" + type + '\'' +
                ", openViolationCount=" + openViolationCount +
                ", challengeQuestion='" + challengeQuestion + '\'' +
                ", challengeAnswer='" + challengeAnswer + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
