package com.example.tollsbyny.model;

import com.example.tollsbyny.model.info.AccountInfoModel;
import com.example.tollsbyny.model.info.FinancialInfoModel;
import com.example.tollsbyny.model.info.PersonalInfoModel;
import com.example.tollsbyny.model.info.ReplenishmentInfoModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountDetailsModel implements Serializable {

    private AccountInfoModel accountInformation;
    private FinancialInfoModel financialInformation;
    private ReplenishmentInfoModel replenishmentInformation;
    private PersonalInfoModel personalInformation;

    public AccountInfoModel getAccountInformation() {
        return accountInformation;
    }

    public void setAccountInformation(AccountInfoModel accountInformation) {
        this.accountInformation = accountInformation;
    }

    public FinancialInfoModel getFinancialInformation() {
        return financialInformation;
    }

    public void setFinancialInformation(FinancialInfoModel financialInformation) {
        this.financialInformation = financialInformation;
    }

    public ReplenishmentInfoModel getReplenishmentInformation() {
        return replenishmentInformation;
    }

    public void setReplenishmentInformation(ReplenishmentInfoModel replenishmentInformation) {
        this.replenishmentInformation = replenishmentInformation;
    }

    public PersonalInfoModel getPersonalInformation() {
        return personalInformation;
    }

    public void setPersonalInformation(PersonalInfoModel personalInformation) {
        this.personalInformation = personalInformation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDetailsModel that = (AccountDetailsModel) o;
        return Objects.equals(accountInformation, that.accountInformation) &&
                Objects.equals(financialInformation, that.financialInformation) &&
                Objects.equals(replenishmentInformation, that.replenishmentInformation) &&
                Objects.equals(personalInformation, that.personalInformation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountInformation, financialInformation, replenishmentInformation, personalInformation);
    }
}
