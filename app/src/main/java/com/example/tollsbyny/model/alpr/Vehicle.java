package com.example.tollsbyny.model.alpr;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Vehicle implements Serializable {

    @JsonProperty("body_type")
    private List<AlprInfo> bodyType;

    private List<AlprInfo> year;

    @JsonProperty("make_model")
    private List<AlprInfo> makeModel;

    @JsonProperty("make")
    private List<AlprInfo> make;

    @JsonProperty("color")
    private List<AlprInfo> color;

    public List<AlprInfo> getBodyType() {
        return bodyType;
    }

    public void setBodyType(List<AlprInfo> bodyType) {
        this.bodyType = bodyType;
    }

    public List<AlprInfo> getYear() {
        return year;
    }

    public void setYear(List<AlprInfo> year) {
        this.year = year;
    }

    public List<AlprInfo> getMakeModel() {
        return makeModel;
    }

    public void setMakeModel(List<AlprInfo> makeModel) {
        this.makeModel = makeModel;
    }

    public List<AlprInfo> getMake() {
        return make;
    }

    public void setMake(List<AlprInfo> make) {
        this.make = make;
    }

    public List<AlprInfo> getColor() {
        return color;
    }

    public void setColor(List<AlprInfo> color) {
        this.color = color;
    }
}
