package com.example.tollsbyny.model.info;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FinancialInfoModel implements Serializable {

    private String financialStatus;
    private String statementDeliveryInterval;
    private String statementDeliveryMethod;
    private BigDecimal tollBalance;
    private BigDecimal violationBalance;
    private BigDecimal currentBalance;

    public String getFinancialStatus() {
        return financialStatus;
    }

    public String getStatementDeliveryInterval() {
        return statementDeliveryInterval;
    }

    public String getStatementDeliveryMethod() {
        return statementDeliveryMethod;
    }

    public BigDecimal getTollBalance() {
        return tollBalance;
    }

    public BigDecimal getViolationBalance() {
        return violationBalance;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setFinancialStatus(String financialStatus) {
        this.financialStatus = financialStatus;
    }

    public void setStatementDeliveryInterval(String statementDeliveryInterval) {
        this.statementDeliveryInterval = statementDeliveryInterval;
    }

    public void setStatementDeliveryMethod(String statementDeliveryMethod) {
        this.statementDeliveryMethod = statementDeliveryMethod;
    }

    public void setTollBalance(BigDecimal tollBalance) {
        this.tollBalance = tollBalance;
    }

    public void setViolationBalance(BigDecimal violationBalance) {
        this.violationBalance = violationBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FinancialInfoModel that = (FinancialInfoModel) o;
        return Objects.equals(financialStatus, that.financialStatus) &&
                Objects.equals(statementDeliveryInterval, that.statementDeliveryInterval) &&
                Objects.equals(statementDeliveryMethod, that.statementDeliveryMethod) &&
                Objects.equals(tollBalance, that.tollBalance) &&
                Objects.equals(violationBalance, that.violationBalance) &&
                Objects.equals(currentBalance, that.currentBalance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(financialStatus, statementDeliveryInterval, statementDeliveryMethod, tollBalance, violationBalance, currentBalance);
    }
}
